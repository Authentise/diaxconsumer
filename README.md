# Authentise JAWS AWSM Module for consuming 3Diax services.

JAWS: https://github.com/jaws-framework/JAWS

See `lib` for actual Node.js code for creating jwt tokens and processing an operation request from 3Diax.
