var action = require('diaxConsumer').createToken;

module.exports.run = function(event, context, cb) {
  var success = function(response) {
    cb(null, response);
  };

  var error = function(error) {
    cb(error, null);
  };

  return action.run(event).then(success).error(error);
};
