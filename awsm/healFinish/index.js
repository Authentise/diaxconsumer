var action = require('diaxConsumer').heal;

module.exports.run = function(event, context, cb) {
  var success = function(response) {
    cb(null, response);
  };

  var error = function(error) {
    cb(error, null);
  };

  return action.heal(event).then(success).error(error);
};
