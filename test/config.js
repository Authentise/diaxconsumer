'use strict';

var chai = require("chai");
var fs = require("fs");
var uuid = require("node-uuid");

chai.config.truncateThreshold = 0;

process.env.DIAX_HOST = "https://integrations.authentise.com";
process.env.DIAX_GROUP_URI = "https://users.authentise.com/groups/<uuid>/";
process.env.DIAX_GROUP_SECRET = fs.readFileSync('test/resources/key.pem');
process.env.DIAX_PUBLIC_CERT = fs.readFileSync('test/resources/cert.pem');
process.env.DIAX_IDENTITY = uuid.v4();
