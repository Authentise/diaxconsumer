var fs = require('fs');
var expect = require("chai").expect;
var jwt = require("../lib/jwt");

describe('jwt', function () {
  describe('#sign', function () {
    it('should return signed jwt', function () {
      var identity = process.env.DIAX_IDENTITY;
      var token = jwt.sign(identity);
      var verifiedToken = jwt.verify(token);

      expect(verifiedToken.sub).to.equal(identity);
      expect(verifiedToken.iss).to.equal(process.env.DIAX_GROUP_URI);
      expect(verifiedToken.aud).to.equal(process.env.DIAX_HOST);
    });
  });
});
