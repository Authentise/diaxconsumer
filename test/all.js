'use strict';

require("./config");

describe('AllTests', function() {

  before(function(done) {
    this.timeout(0);  //dont timeout anything
    done();
  });

  after(function() {
  });

  require('./jwt');
  require('./createToken');
  require('./heal');
});
