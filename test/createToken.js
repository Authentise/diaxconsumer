var expect = require("chai").expect;
var createToken = require("../lib/createToken");
var jwt = require("../lib/jwt");

describe('createToken', function () {
  var data = {
    identity: process.env.DIAX_IDENTITY
  };

  describe('#validate', function () {
    it('should return valid data', function () {
      return createToken.validate(data).then(function(result){
        expect(result).to.equal(data);
      });
    });
  });

  describe('#createToken', function () {
    it('should return token payload', function () {
      return createToken.createToken(data).then(function(result){
        expect(result.token).to.be.a('string');
        var body = jwt.verify(result.token);
        expect(body.sub).to.equal(data.identity);
      });
    });
  });

  describe('#run', function () {
    it('should return valid payload', function () {
      return createToken.run(data).then(function(result){
        expect(result.token).to.be.a('string');
        var body = jwt.verify(result.token);
        expect(body.sub).to.equal(data.identity);
      });
    });
  });
});
