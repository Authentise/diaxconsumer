var Promise = require('bluebird');
var jwt = require('./jwt');

var validate = function(data) {
  return new Promise(function(resolve, reject) {
    resolve(data);
  });
};

var createToken = function(data) {
  return new Promise(function(resolve, reject) {
    resolve({
      token: jwt.sign(process.env.DIAX_IDENTITY)
    });
  });
};

var run = function(event) {
  return validate(event).then(createToken)
};

module.exports = {
  validate: validate,
  createToken: createToken,
  run: run
};
