var jwt = require('jsonwebtoken');
var extend = require('util')._extend;

var sign = function(identity, options) {
  var signOptions = extend(options || {}, {
    algorithm: 'RS512',
    expiresIn: "1d",
    subject: identity,
    issuer: process.env.DIAX_GROUP_URI,
    audience: process.env.DIAX_HOST,
  });

  var timestamp = Math.floor((Date.now() / 1000) - 5000);

  var body = {
    identity: identity,
    nbf: timestamp,
    iat: timestamp,
  };

  return jwt.sign(body, process.env.DIAX_GROUP_SECRET, signOptions);
};

var verify = function(token, options, callback) {
  var verifyOptions = extend(options || {}, {
    algorithms: ['RS512'],
  });

  return jwt.verify(token, process.env.DIAX_PUBLIC_CERT, verifyOptions, callback);
};

module.exports.sign = sign;
module.exports.verify = verify;
