var Promise = require('bluebird');
var jwt = require('./jwt');
var fetch = require('node-fetch');

fetch.Promise = require('bluebird');

var validate = function(data) {
  console.log("REQUEST DATA", data);

  return new Promise(function(resolve, reject) {
    if(!data.jwt) {
      console.log("NO TOKEN", data);
      reject("NO TOKEN");
      return
    }

    if(!data.content) {
      console.log("NO CONTENT", data);
      reject("NO CONTENT");
      return
    }

    if(!data.operation) {
      console.log("NO OPERATION", data);
      reject("NO OPERATION");
      return
    }

    jwt.verify(data.jwt, null, function(err, body) {
      if(err) {
        console.log("ERROR VERIFYING TOKEN", body, err);
        reject({ message: "ERROR VERIFYING TOKEN", error: err.message });
      } else if (body.sub !== process.env.DIAX_IDENTITY) {
        console.log("Mismatch identity", body, err, process.env.DIAX_IDENTITY);
        reject({message : "Mismatch identity"});
      } else {
        console.log("Verify Complete", body);
        resolve(data);
      }
    });
  });
};

var heal = function(data) {
  var headers = {
    "content-type": "application/json",
    "jwt": jwt.sign(process.env.DIAX_IDENTITY)
  };

  var body = {
    status: "complete",
    content: 'https://dl.dropboxusercontent.com/u/20616489/40mmcube.stl'
  };

  console.log("CALLBACK HOTH", data, headers, body);
  return new Promise(function(resolve, reject) {
    fetch(data.operation, { method: 'PUT', body: JSON.stringify(body), headers: headers })
      .then(function(response) {
        console.log("CALLBACK SUCCESS", response);
        resolve("DONE");
      }).error(function(err) {
        console.log("CALLBACK FAILED", err);
        reject("CALLBACK FAILED");
      });
  });
};

module.exports = {
  validate: validate,
  heal: heal
};
